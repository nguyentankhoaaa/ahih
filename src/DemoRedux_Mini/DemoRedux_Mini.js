import React, { Component } from "react";
import { connect } from "react-redux";

class DemoRedux_Mini extends Component {
  render() {
    console.log(this.props);
    console.log(this.props);
    return (
      <div>
        <button className="btn btn-danger"
        onClick={this.props.handleGiamSoLuong}>-</button>
        <strong className="mx-5">{this.props.soLuong}</strong>
        <button
          onClick={this.props.handleTangSoLuong}
          className="btn btn-success"
        >
          +
        </button>
      </div>
    );
  }
}



let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };

};
let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      let action = {
        type: "TANG_SO_LUONG",
      };
      dispatch(action);
    },
    handleGiamSoLuong:()=>{
      let action ={
        type:"GIAM_SO_LUONG",
        payload:5,
      };
      dispatch(action)
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DemoRedux_Mini);
