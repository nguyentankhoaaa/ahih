import "./App.css";
import DemoRedux_Mini from "./DemoRedux_Mini/DemoRedux_Mini";

function App() {
  return (
 
    <div className="App">
    
      <DemoRedux_Mini />
  
    </div>
  );
}
/*  */
export default App;
